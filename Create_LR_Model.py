#!/usr/bin/env python
'''Create A Linear Regression Model

This file can be run to create and score a Polynomial implementation of a
Linear Regression model.  It can be saved as a pickled file for future use, or
run without saving to find performance metrics.

It also contains a function to cross validate a variety of potential models,
used in initial exploration of the problem.

The training data is presumed to be saved in './data/dataLR_{}day.pkl' where {}
is the number of days in each observation group.

ToDo:
    Implement ArgParse to accept options from the commnad line.
    Comment the code further.
'''

import pickle
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression, ElasticNet
from sklearn.preprocessing import PolynomialFeatures
from sklearn.ensemble import AdaBoostRegressor, RandomForestRegressor, \
     GradientBoostingRegressor
from matplotlib import pyplot as plt
from matplotlib import dates as mpldates


__author__ = "Ben Doremus"
__credits__ = "Ben Doremus, Matthew Mcelhaney"
__email__ = "bdoremus@gmail.com"
__status__ = "Prototype"


def main(max_poly=1, save_m=False, num_days=1, print_model=False):
    '''
    Create and score a linear regression model.

    Args:
        max_poly (int): The polynomial to use.  1 is simple Linear Regression,
                        2 is quadratic, 3 is trinomial, etc.
        save_m (bool):  Set to True in order to save the pickled model.
        num_days (int): The number of days in each observation group.  This
                        determines which data file to use.
        print_model (bool): Set to True to print the regression coefficients

    Returns:
        None
    '''
    X_train, X_test, y_train, y_test, X, y = loadPickledData(num_days)
    # PolynomialFeatures(degree=2, include_bias=False),
    model = make_pipeline(StandardScaler(),
                          PolynomialFeatures(degree=max_poly,
                                             include_bias=False),
                          LinearRegression(fit_intercept=True,
                                           normalize=False,
                                           n_jobs=-1))
    model.fit(X_train, y_train)

    score_model(model, X_test, y_test)

    # Print the coefficients
    if print_model:
        coefs = model.steps[2][1].coef_
        x_int = model.steps[2][1].intercept_
        coef_total = sum(abs(coefs))+abs(x_int)
        poly_names = model.steps[1][1].get_feature_names(X_train.columns)
        print("Coefficients:")
        print("| X_int (anadarko) | {:.2f}% |  ".format(100*x_int/coef_total))
        for n, c in zip(poly_names, coefs):
            if True:  # abs(c/coef_total) > 0.01:
                n = str.strip(n)
                n = n.replace(" ", " * ")
                print("| {} | {:.2f}% |  ".format(n, 100*c/coef_total))

    if save_m:
        filename = './model_n{}.pkl'.format(max_poly)
        with open(filename, 'wb') as f:
            pickle.dump(model, f)
            print("Model saved as ", filename)

    if print_model:
        graph_model(X, y, model)
    return None


def loadPickledData(num_days):
    '''
    Load the data from the presumed location.

    Args:
        num_days (int): The number of days in each observation group.  This
                        determines which data file to use.

    Returns:
        X_train, X_test, y_train, y_test (pandas df): Train/Test split data
        X, y (pandas df): The original X and y dataframes
    '''
    filename = './data/dataLR_{}day.pkl'.format(num_days)
    with open(filename, 'rb') as f:
        X = pickle.load(f)
        y = X.pop('failureRate').values
    X_train, X_test, y_train, y_test = train_test_split(X,
                                                        y,
                                                        test_size=0.2,
                                                        shuffle=True,
                                                        random_state=42,
                                                        stratify=(y > 0))
    return X_train, X_test, y_train, y_test, X, y


def score_model(model, X_test, y_test):
    '''
    Print relevant metrics for a given model.

    Args:
        model: Any model that implements the predict function.  Typically a
               SKLearn pipeline of different transformations and an estimator.
        X_test, y_test (pandas df): Test data to score against.

    Returns:
        None
    '''
    y_pred = model.predict(X_test)
    # If predictions are less than 0, set them to 0
    y_pred[y_pred < 0] = 0

    print("Mean Absolute Error: {:.2f}".format(
          mean_absolute_error(y_test, y_pred)))
    print("Root Mean Squared Error: {:.2f}".format(
          mean_squared_error(y_test, y_pred)**0.5))
    print("R^2 Score: {:.2f}".format(
          r2_score(y_test, y_pred)))

    return None


def crossval_trials(num_days):
    '''
    Explore and compare metrics from a couple different models.
    Loads the data from default location.  Cross validate using the train data.
    Prints results to screen for a variety of models and metrics.

    Args:
        num_days (int):  The number of days in each observation group.  This
                         determines which data file to use.

    Returns:
        None
    '''
    X_train, X_test, y_train, y_test, X, y = loadPickledData(num_days)

    # Scale X
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test - scaler.transform(X_test)

    # Models to test using cross validation
    linear = LinearRegression()
    enet = ElasticNet()
    poly2 = make_pipeline(PolynomialFeatures(degree=2),
                          LinearRegression(fit_intercept=True,
                                           normalize=False))
    poly2_e = make_pipeline(PolynomialFeatures(degree=2),
                            ElasticNet(fit_intercept=True,
                                       normalize=False))
    poly3 = make_pipeline(PolynomialFeatures(degree=3),
                          LinearRegression(fit_intercept=True,
                                           normalize=False))
    poly3_e = make_pipeline(PolynomialFeatures(degree=3),
                            ElasticNet(fit_intercept=True,
                                       normalize=False))

    # Create a list of model over which we can iterate
    model_types = [linear,
                   enet,
                   poly2,
                   poly2_e,
                   poly3,
                   poly3_e,
                   DecisionTreeRegressor(),
                   AdaBoostRegressor(),
                   RandomForestRegressor(),
                   GradientBoostingRegressor()]

    # Print cross val scores for each model
    print("| Type | r2 | explained variance | Mean Abs Err | RMSE |  ")
    print("| ---- | -- | ------------------ -------------- | ---- | ")
    for m in model_types:
        r2 = cross_val_score(m,
                             X_train,
                             y_train,
                             cv=3,
                             scoring='r2',
                             n_jobs=-1).mean()
        mae = -1 * cross_val_score(m,
                                   X_train,
                                   y_train,
                                   cv=3,
                                   coring='neg_mean_absolute_error',
                                   n_jobs=-1).mean()
        mse = cross_val_score(m,
                              X_train,
                              y_train,
                              cv=3,
                              scoring='neg_mean_squared_error',
                              n_jobs=-1).mean()
        rmse = (-1 * mse)**0.5

        # Create a string for the model used and the polynomial degree
        if m.__class__.__name__ == 'Pipeline':
            name = m.steps[1][1].__class__.__name__ + \
                   " n^" + str(m.steps[0][1].degree)
        else:
            name = m.__class__.__name__
        # Remove uninformative "Regressor" for aesthetic reasons
        name.replace('Regressor', '')

        # Print one line of results for the given model "m"
        print("| {} | {:.2f} | {:.2f} | {:.2f} |  ".format(
                name,   r2,     mae,     rmse))

        return None


def graph_model(X, y, model):
    '''
    Create a line graph with both the predicted and expected results.

    Args:
        X, y (pandas df): Input and output dataframes
        model: Any model that implements the predict function.  Typically a
               SKLearn pipeline of different transformations and an estimator.

    Returns:
        None
    '''

    y_pred = model.predict(X)
    # If predictions are less than 0, set them to 0
    y_pred[y_pred < 0] = 0

    fig = plt.figure(figsize=(12, 7.5), dpi=120)
    bu_masks, businessUnits = get_bu_masks(X)
    for aNum, (bu_mask, bu) in enumerate(zip(bu_masks, businessUnits)):
        ax = fig.add_subplot(3, 2, aNum+1)
        y_vals = y[bu_mask]
        y_pred_vals = y_pred[bu_mask]
        x_vals = X.index.date[bu_mask]

        ax.plot(x_vals, y_vals, 'r', label='True')
        ax.plot(x_vals, y_pred_vals, 'b', label='Prediction')

        ax.xaxis.set_major_formatter(mpldates.DateFormatter('%b'))
        ax.set_ylabel('Failure Proportion')
        ax.set_title(bu.upper())
        ax.set_ylim(0, y.max())
        ax.legend(loc='upper right')

    fig.tight_layout()
    # plt.suptitle("Proportion of compressors that fail \
    #               in each asset region")
    # plt.suptitle("Proportion of compressors that fail \
    #               in each asset region\n\
    #               Predictions using LinearRegression with \
    #               a polynomial degree of {}".format(
    #               model.steps[1][1].degree))
    plt.show()

    return None


def get_bu_masks(X):
    '''
    When splitting the data into different asset regions, it's helpful to have
    a mask that corresponds to each region.

    Args:
        X (pandas df): A dataframe with one dummy column for each asset region.

    Returns:
        bu_masks (2D list of booleans): Each item of the list is a boolean mask
                                        relating to each asset region
        businessUnits (list of strings): The list of regions corresponding to
                                         the bu_masks
    '''

    # anadarko was dropped as a dummy
    businessUnits = ['arkoma',
                     'durango',
                     'easttexas',
                     'farmington',
                     'wamsutter']

    bu_masks = [(X[bu] > 0) for bu in businessUnits]
    # add a mask for anadarko
    bu_masks.append((X['arkoma'] <= 0) &
                    (X['durango'] <= 0) &
                    (X['easttexas'] <= 0) &
                    (X['farmington'] <= 0) &
                    (X['wamsutter'] <= 0))

    # Add in the dummy that was dropped
    businessUnits.append('anadarko')

    return bu_masks, businessUnits


if __name__ == '__main__':
    '''
    Change these parameters to adjust how the file is run.
    Eventually, will change main() so that it can be run using ArgParse from
    the command line.
    '''
    # Options for running this file
    save_m = True
    print_model = True
    max_poly = 2
    num_days = 7

    # crossval_trials(num_days)
    main(max_poly, save_m, num_days, print_model)
