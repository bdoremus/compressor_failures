# If It Ain't Broke, Fix It?

## The Problem
**Predicting compressor failures**

Gas and Oil wells have many moving parts, and a compressor is often central to the system.  They're used for everything from fracking to transportation, and vary greatly in size and complexity.  Regardless of use case, it's a major problem when they break.

Knowledge about why compressors fail is often very localized; a technician in Wyoming doesn't often speak with a peer in Texas.  Data about the failures is equally isolated, with regions that don't compare notes about their hardware.  By combining information from all of the regions and incorporating relevant weather data, it will be easier to discover the particulars affecting each region and **provide insight into the relative performance of compressors across the continental US.**

Further, using the weather forecast will provide estimates on future compressor failures with the aim of improving uptime and stabilizing the flow of hydrocarbons from the well.

# Creating the Dataset
**Goal: Know how many compressors failed in each asset region every day.**  
Creating the dataset which can answer our problem was, in itself, a challenge.  All told, I combined 14 different files with several 'unique' identifiers to be able to map the daily weather in an asset region to the number of failures.

Below is an outline for how the data was joined.  This is unfortunately important, as I have to throw out a large number of potentially useful features since there was no way to preserve that information when combining the different files.

![File Map](./img/DataStructure_04.png)  

### A Usable Dataset
Combining all that together, there now exists a target y (daily proportion of compressors that failure/asset region) which can be predicted based on some features X (weather and location).  
![Combined data sets](./img/DataStructure_05.png)  

### Basic Feature Engineering
Interestingly, of the weather data, the daily high temperature proved by far the most predictive feature.  Given that having colinear variables violates the base assumptions of Linear Regression, the model improved in every metric when I dropped the other weather features.

# Data Exploration
Here is what the failure rates look like over time, broken down by asset region.  To smooth out the noise and make predictions more useful, failures were summed into one-week increments.
![Weekly failures per asset region](./img/weekly_failure_rate.png)

There seems to be a very clear correlation between low temperatures and high failure rates.  Looking at one business unit with the cleanest data, we can see a very clear inverse linear relationship.  
![Temperature inversely related to failure rates](./img/FailuresVsTemp.png)

# Model Choice
While there are many models which may perform well, there are a few factors that dictate the most appropriate direction:  
* Given such a small feature space, models that easily overfit would be exceptionally bad.  
* The utility of the model will come from its explanatory ability.  
* As this is time series data, we would prefer a model that can account for it.  However, we are limited by only having less than a full year of information.

Given these limitations, Linear Regression was one obvious choice.  However, it comes with some assumptions that need to be addressed.

## Linear Regression
### Relevant Assumptions
1. **No colinearity between features**  
After preliminary feature exploration, it became evident that the high temperature was by far the most predictive of the weather information.  This implies that prolonged cold is far more destructive than sudden but brief drops in temperature.  To reduce colinearity, the other weather features were dropped.

1. **The signal is linear**  
I suspected that this was not true, and that it was more of a parabolic relationship where lower temperatures lead to ever-increasing likelihood of failure.  Therefore, I manufactured a max_temperature^2 variable.  See Results below.

1. **Homoscedasticity**  
It does appear that variance is equally distributed across temperature ranges, but not across time.  Given my distrust of some of the data, I would want to re-examine this with more reliable data to ensure it still holds true.

### Further Feature engineering
Given the max_temp^2 feature, I created dummy interaction variables with every asset region.  So the final dataset had:  
* A date index  
* An y-intercept (anadarko was the dropped dummy column)  
* A 1/0 for each asset region  
* A max_temp * asset region dummy column  
* A max_temp^2 * asset region dummy column  

For each asset region, there are then 3 values affecting the prediction: the y-intercept

### Results
The model was trained on 252 rows of data.  Each row was one week's failure proportion in each Asset Region.  The data was split 80/20 into train/test, with cross validation on the 80% training data performed to choose the best features and architecture.

| | n^2 LinearRegression |
| -------- | --------------------- |
| **R^2**  | 0.53        |
| **MAE**  | 0.09                  |
| **RMSE** | 0.13                  |

#### Predicted Failure Rates Vs Temperature
![Predicted Failures vs Temperature](./img/PredictedFailuresVsTemp.png)

#### Coefficient Analysis
By interpreting the magnitude of the coefficients and shape of the resulting paraboloid, we can compare business units' resilience to cold temperature.  (Actual coefficients can be found in the appendix)

| Asset Region | Base Failure (high is bad) | Temperature vulnerability (high is bad) |
| ------------ | -------------------------- | --------------------------------------- |
| Anadarko     | Very Low                   | Very High                               |
| Arkoma       | Low                        | Medium                                  |
| Durango      | Very Low                   | Very Low                                |
| East Texas   | Very Low                   | Very High                               |
| Farmington   | Medium                     | Very Low                                |
| Wamsutter    | Very High                  | Medium                                  |

*These results should be reverified with other data sources before making decisions based on these conclusions*

## Gaussian Process Regressor
While Linear Regression proved to be useful even with a limited number of features, it omits some potentially helpful information: time.  It seems likely that prior failures affect future results, so a Bayesian approach which works with time series data would be appropriate.

**Gaussian Processes** are distributions over functions, and that distribution represents the likelihood of a certain prediction given information about the prior states.  In addition to performing a prediction (the mean value of the distribution at a given time), this type of model also provides a confidence interval for that prediction.

The hyper-parameters for this non-parametric model are kernels.  A kernel is compiled of different distributions to represent the effect prior data points has on the posterior prediction.  In this case, a Rational Quadratic kernel was combined with a White Noise kernel.

Rational Quadratic kernels are:
```
 "...a scale mixture of squared exponential kernels with different length scales.
 This gives variations with a range of time-scales, the distribution peaking around
λ but extending to significantly longer period (but remaining rather smooth)"  
(http://www.robots.ox.ac.uk/~sjrob/Pubs/philTransA_2012.pdf)
```

### Next Week's Predictions
Predicted failures for next week (10/3 - 10/10) using Gaussian Process Regressor.

| Location | Predicted Failure Rate |
| - | - |
| arkoma |     10.7% |
| durango |    00.0% |
| easttexas |  07.5% |
| farmington | 02.7% |
| wamsutter |  28.9% |
| anadarko |   05.6% |

![Forecast Failures](./img/ForecastFailures10-3_10-10.png)

## Really bad assumptions
Due to limitations in the way the data was recorded and aggregated, many potentially flawed assumptions were made.  If further work is to be done on this project, these assumptions should be specifically addressed.
* The date that the failure was recorded was the same as the date of the failure
* The weather at the general asset region station is indicative of the weather for all compressors in the asset region
* The number of compressors in each asset region is constant throughout the year
* Different compressors are similarly affected by weather
* Weather is the only factor affecting compressor failures
* Summarizing the data by asset region is the best way to predict and view results

## Breaking the model
The choice is model was made to aid in comparing across asset regions.  However, other models may perform better due to restrictions inherent in Linear Regression and Gaussian Processes.  Specifically:
* LinearRegression assumes:
  * no colinearity: max/min temps are highly correlated
  * the signal is linearly related to the features: <0% or >100% failures are impossible
  * homoscedasticity: extreme temperatures seem to have more variance
* Gaussian Processes assumes an underlying repeatable distribution: trained on <1 full period of data
* Gaussian Processes works with time series data: the distribution of failures through time makes it tough to make a meaningful test/train
* Lots of training data is needed: weekly totals have 252 data points

# Improvements
If further progress is to be made on this project, here is where I suggest beginning:
1. Reassess with multiple years of data, enabling a more accurate Gaussian Process
1. Refactor the code into PyMC3 with Gaussian Process
  * Allows for a multilevel model with modifications for each asset region
  * Allows for more inspection of the relative importance of the features
1. Add more features pertinent to compressor failures
  * Fractional distribution of manufacturers
  * Proportion of well types
1. Predict failures on a per-compressor level, as opposed to asset region wide.  Look at much more relevant features
  * Time between maintenance
  * Physical specs: RPM, noise, line pressure, etc
  * Make/Model and year

# About Me
https://www.linkedin.com/in/bdoremus/

As a Data Scientist with a degree in Electrical and Computer Engineer and 10 years of experience teaching, I bring a unique skill set to any Data Science team. Combining my strong technical background in software and mathematics with the daily experiences I had leading a classroom, I have a proven ability to communicate with all stakeholders, be they technically adept or focused on other fields. My hobbies tend to inform my work in interesting ways, be it through music, microcontrollers, or woodcraft.

# Appendix
## Coefficients for n^2 Linear Regression
| Term | Coefficient |
| -------- | -------- |
| Y_intercept (anadarko) | 1.79 |  
| maxTemp | -0.39 |  
| arkoma | -0.53 |  
| durango | -0.72 |  
| easttexas | 0.23 |  
| farmington | -0.73 |  
| wamsutter | -0.38 |  
| maxTemp_anadarko | -0.66 |  
| maxTempSqrd_anadarko | 0.59 |  
| maxTemp_arkoma | 0.43 |  
| maxTempSqrd_arkoma | 0.04 |  
| maxTemp_durango | 0.48 |  
| maxTempSqrd_durango | 0.01 |  
| maxTemp_easttexas | -1.16 |  
| maxTempSqrd_easttexas | 0.87 |  
| maxTemp_farmington | 0.65 |  
| maxTempSqrd_farmington | -0.05 |  
| maxTemp_wamsutter | 0.05 |  
| maxTempSqrd_wamsutter | 0.26 |
